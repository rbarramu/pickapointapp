//
//  Alert.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 31-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    // alert
    func presentAlertView(alertViewType type: AlertViewTypes) {
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .alert)
        var alertTitle: String = ""
        var alertMessage: String = ""
        var acceptTitle: String = ""

        switch type {
        case .serverError:
            alertTitle = ""
            alertMessage = Messages.UIAlertView.serverErrorText
            acceptTitle = Messages.UIAlertView.okText
        case .noInternetAlert:
            alertTitle = ""
            alertMessage = Messages.UIAlertView.internetText
            acceptTitle = Messages.UIAlertView.okText

        default:
            break
        }

        alertController.addAction(UIAlertAction(title: acceptTitle, style: .default, handler: nil))

        present(alertController, animated: true, completion: nil)
    }
}
