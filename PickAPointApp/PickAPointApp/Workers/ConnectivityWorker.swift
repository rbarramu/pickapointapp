//
//  ConnectivityWorker.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 29-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Alamofire
import Foundation

class ConnectivityWorker {
    class var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
