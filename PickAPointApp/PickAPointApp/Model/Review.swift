//
//  Review.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 31-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import RealmSwift

class Review: Object {
    @objc dynamic var remoteID = UUID().uuidString
    @objc dynamic var authorName = ""
    @objc dynamic var authorText = ""
    @objc dynamic var authorImage = ""
    @objc dynamic var rating = 0.0

    override static func primaryKey() -> String? {
        return "remoteID"
    }
}

extension Review {
    func save() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }
    }

    class func all() -> Results<Review> {
        let realm = try! Realm()
        return realm.objects(Review.self)
    }

    func delete() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self)
        }
    }
}
