//
//  Favorite.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 31-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import RealmSwift

class Place: Object {
    @objc dynamic var remoteID = ""
    @objc dynamic var name = ""
    @objc dynamic var address = ""
    @objc dynamic var referenceImage = ""
    @objc dynamic var rating = 0.0
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    var reviews = List<Review>()

    override static func primaryKey() -> String? {
        return "remoteID"
    }
}

extension Place {
    func save() {
        let realm = try! Realm()
        try! realm.write {
            realm.add(self)
        }
    }

    class func all() -> Results<Place> {
        let realm = try! Realm()
        return realm.objects(Place.self)
    }

    func delete() {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(self)
        }
    }

    class func searchPlace(id: String) -> Place {
        let realm = try! Realm()
        let place = realm.objects(Place.self).filter("remoteID == %@", id).first
        return place!
    }

    class func statusPlace(id: String) -> Bool {
        let realm = try! Realm()
        let places = realm.objects(Place.self).filter("remoteID == %@", id)
        if places.count == 0 {
            return false
        } else {
            return true
        }
    }

    class func addNewPlace(detail: NSDictionary) {
        let realm = try! Realm()

        let name = detail["name"] as? String ?? ""
        let formatted_address = detail["formatted_address"] as? String ?? ""
        let icon = detail["icon"] as? String ?? ""
        let rating = detail["rating"] as? Double ?? 0
        let geometry = detail["geometry"] as? NSDictionary ?? [:]
        let location = geometry["location"] as? NSDictionary ?? [:]
        let lat = location["lat"] as? Double ?? 0.0
        let lng = location["lng"] as? Double ?? 0.0
        let place_id = detail["place_id"] as? String ?? ""

        try! realm.write {
            let newPlace = Place()

            newPlace.name = name
            newPlace.address = formatted_address
            newPlace.referenceImage = icon
            newPlace.rating = rating
            newPlace.latitude = lat
            newPlace.longitude = lng
            newPlace.remoteID = place_id

            let reviews = detail["reviews"] as? NSArray ?? []

            for review in reviews {
                let reviewDict = review as! NSDictionary

                let author_name = reviewDict["author_name"] as? String ?? ""
                let text = reviewDict["text"] as? String ?? ""
                let profile_photo_url = reviewDict["profile_photo_url"] as? String ?? ""
                let rating = reviewDict["rating"] as? Double ?? 0

                let newReview = Review()

                newReview.authorName = author_name
                newReview.authorText = text
                newReview.authorImage = profile_photo_url
                newReview.rating = rating

                newPlace.reviews.append(newReview)
            }

            realm.add(newPlace)
        }
    }

    func toDictionary() -> NSDictionary {
        var reviews = [NSDictionary]()

        for review in self.reviews {
            let dictReview: NSDictionary = ["author_name": review.authorName, "text": review.authorText, "profile_photo_url": review.authorImage, "rating": review.rating]
            reviews.append(dictReview)
        }

        let dict: NSDictionary = ["name": name, "icon": referenceImage, "rating": rating, "place_id": remoteID, "formatted_address": address, "geometry": ["location": ["lat": latitude, "lng": longitude]], "reviews": reviews]
        return dict
    }
}
