//
//  InformationCell.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 30-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Cosmos
import Foundation
import UIKit

class InformationCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var favouriteButton: UIButton!
    @IBOutlet var referenceImageView: UIImageView!
    @IBOutlet var ratingView: CosmosView!

    var counter = 0
    var place_id = ""

    var detail: NSDictionary? {
        willSet(newValue) {
            self.detail = newValue
        }
        didSet {
            self.configurateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configurateCell() {
        let name = detail!["name"] as? String ?? ""
        let icon = detail!["icon"] as? String ?? ""
        let rating = detail!["rating"] as? Double ?? 0
        place_id = detail!["place_id"] as? String ?? ""

        nameLabel.text = name
        nameLabel.textColor = .black
        nameLabel.font = UIFont.boldSystemFont(ofSize: 14)
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.numberOfLines = 2

        let url = URL(string: icon)

        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url!) {
                DispatchQueue.main.async {
                    self?.referenceImageView.image = UIImage(data: data)
                }
            }
        }

        if Place.statusPlace(id: place_id) {
            favouriteButton.setImage(Constants.Images.heartSelectedIcon?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), for: .normal)
            counter = counter + 1
        } else {
            favouriteButton.setImage(Constants.Images.heartUnselectedIcon?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), for: .normal)
            counter = 0
        }

        favouriteButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)

        // Change the cosmos view rating
        ratingView.rating = rating

        // Do not change rating when touched
        // Use if you need just to show the stars without getting user's input
        ratingView.settings.updateOnTouch = false

        // Change the text
        ratingView.text = String(rating)
    }

    @objc func buttonPressed(sender _: UIButton!) {
        if counter == 0 {
            favouriteButton.setImage(Constants.Images.heartSelectedIcon?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), for: .normal)
            counter = counter + 1
            Place.addNewPlace(detail: detail!)

        } else {
            favouriteButton.setImage(Constants.Images.heartUnselectedIcon?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal), for: .normal)
            counter = 0

            if Place.statusPlace(id: place_id) {
                Place.searchPlace(id: place_id).delete()
            }

            NotificationCenter.default.post(name: Notification.Name("refreshTable"), object: nil)
        }
    }
}
