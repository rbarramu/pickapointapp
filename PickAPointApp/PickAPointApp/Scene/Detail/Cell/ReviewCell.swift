//
//  ReviewCell.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 30-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Cosmos
import Foundation
import UIKit

class ReviewCell: UITableViewCell {
    @IBOutlet var authorNameLabel: UILabel!
    @IBOutlet var authorTextLabel: UILabel!
    @IBOutlet var authorImageView: UIImageView!
    @IBOutlet var ratingView: CosmosView!

    var detail: NSDictionary? {
        willSet(newValue) {
            self.detail = newValue
        }
        didSet {
            self.configurateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configurateCell() {
        let author_name = detail!["author_name"] as? String ?? ""
        let text = detail!["text"] as? String ?? ""
        let profile_photo_url = detail!["profile_photo_url"] as? String ?? ""
        let rating = detail!["rating"] as? Double ?? 0

        authorNameLabel.text = author_name
        authorNameLabel.textColor = .black
        authorNameLabel.font = UIFont.boldSystemFont(ofSize: 14)
        authorNameLabel.lineBreakMode = .byWordWrapping
        authorNameLabel.numberOfLines = 2

        authorTextLabel.text = text
        authorTextLabel.textColor = .black
        authorTextLabel.font = UIFont.systemFont(ofSize: 14)
        authorTextLabel.lineBreakMode = .byWordWrapping
        authorTextLabel.numberOfLines = 2

        if profile_photo_url != "" {
            let url = URL(string: profile_photo_url)

            DispatchQueue.global().async { [weak self] in
                if let data = try? Data(contentsOf: url!) {
                    DispatchQueue.main.async {
                        self?.authorImageView.image = UIImage(data: data)
                    }
                }
            }
        }

        // Change the cosmos view rating
        ratingView.rating = rating

        // Do not change rating when touched
        // Use if you need just to show the stars without getting user's input
        ratingView.settings.updateOnTouch = false

        // Change the text
        ratingView.text = String(rating)
    }
}
