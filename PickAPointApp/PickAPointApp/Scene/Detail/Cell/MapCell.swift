//
//  MapCell.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 30-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import MapKit

class MapCell: UITableViewCell, MKMapViewDelegate {
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var addressImageView: UIImageView!
    @IBOutlet var mapView: MKMapView!

    let locationManager = CLLocationManager()
    private var latitude = Double()
    private var longitude = Double()

    var detail: NSDictionary? {
        willSet(newValue) {
            self.detail = newValue
        }
        didSet {
            self.configurateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configurateCell() {
        let name = detail!["name"] as? String ?? ""
        let formatted_address = detail!["formatted_address"] as? String ?? ""
        let geometry = detail!["geometry"] as? NSDictionary ?? [:]
        let location = geometry["location"] as? NSDictionary ?? [:]
        let lat = location["lat"] as? Double ?? 0.0
        let lng = location["lng"] as? Double ?? 0.0

        let pin = MKPointAnnotation()
        pin.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        pin.title = name
        mapView.addAnnotation(pin)
        mapView.delegate = self

        updateMapForCoordinate(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lng))

        addressLabel.text = formatted_address
        addressLabel.textColor = .black
        addressLabel.font = UIFont.systemFont(ofSize: 14)
        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.numberOfLines = 2

        addressImageView.image = Constants.Images.locationIcon
    }

    func updateMapForCoordinate(coordinate: CLLocationCoordinate2D) {
        let camera = MKMapCamera(lookingAtCenter: coordinate, fromDistance: 2000, pitch: 0, heading: 0)
        mapView.setCamera(camera, animated: false)
        var center = coordinate
        center.latitude -= mapView.region.span.latitudeDelta / 6.0
        mapView.setCenter(center, animated: false)
    }
}

// MARK: - CLLocationManagerDelegate

extension MapCell: CLLocationManagerDelegate {
    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }

        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
        let centre = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let span = MKCoordinateSpan(latitudeDelta: 0.2, longitudeDelta: 0.2)
        let region = MKCoordinateRegion(center: centre, span: span)
        mapView.setRegion(region, animated: true)
    }
}
