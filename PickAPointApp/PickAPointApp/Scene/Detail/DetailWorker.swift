//
//  DetailWorker.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 30-08-20.
//  Copyright (c) 2020 Rocio Barramuño. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import Alamofire
import UIKit

class DetailWorker {
    func getPlace(place_id: String, completionHandler: @escaping (_ isOk: Bool, _ detail: NSDictionary, _ message: String) -> Void) {
        let parameters: Parameters = [
            "place_id": place_id,
            "key": Api.Server.key,
        ]

        let urlString = Api.Server.debugURL ? Api.Server.testUrl : Api.Server.baseURL
        let apiUrl = urlString + Api.Server.version + Api.Server.details

        guard let url = URL(string: apiUrl) else {
            completionHandler(false, [:], Messages.Api.serverErrorText)
            return
        }

        AF.request(url,
                   method: .get,
                   parameters: parameters)
            .validate()
            .responseJSON { response in
                switch response.result {
                case let .success(value):
                    let jsonValue = value as! NSDictionary
                    let detail = jsonValue["result"] as? NSDictionary ?? [:]
                    completionHandler(true, detail, "")
                case .failure:
                    completionHandler(false, [:], Messages.Api.serverErrorText)
                }
            }
    }
}
