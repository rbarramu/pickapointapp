//
//  FavoritesInteractor.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 31-08-20.
//  Copyright (c) 2020 Rocio Barramuño. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol FavoritesBusinessLogic {
    func selectFavorite(request: Favorites.SelectFavorite.Request)
}

protocol FavoritesDataStore {
    var placeFavorite: Place { get set }
}

class FavoritesInteractor: FavoritesBusinessLogic, FavoritesDataStore {
    var presenter: FavoritesPresentationLogic?
    var worker: FavoritesWorker?
    var placeFavorite = Place()

    // MARK: Select Favorite

    func selectFavorite(request: Favorites.SelectFavorite.Request) {
        placeFavorite = request.place
        let response = Favorites.SelectFavorite.Response()
        presenter?.presentFavorite(response: response)
    }
}
