//
//  FavoriteCell.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 31-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import UIKit

class FavoriteCell: UITableViewCell {
    @IBOutlet var referenceImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressImageView: UIImageView!
    @IBOutlet var addressLabel: UILabel!

    var place: Place? {
        willSet(newValue) {
            self.place = newValue
        }
        didSet {
            self.configurateCell()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configurateCell() {
        let url = URL(string: place!.referenceImage)

        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url!) {
                DispatchQueue.main.async {
                    self?.referenceImageView.image = UIImage(data: data)
                }
            }
        }

        nameLabel.text = place?.name
        nameLabel.textColor = .black
        nameLabel.font = UIFont.boldSystemFont(ofSize: 14)
        nameLabel.lineBreakMode = .byWordWrapping
        nameLabel.numberOfLines = 2

        addressImageView.image = Constants.Images.locationIcon

        addressLabel.text = place?.address
        addressLabel.textColor = .black
        addressLabel.font = UIFont.systemFont(ofSize: 12)
        addressLabel.lineBreakMode = .byWordWrapping
        addressLabel.numberOfLines = 2
    }
}
