//
//  CustomAnnotation.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 30-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import MapKit

class CustomAnnotation: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var id: String?

    init(title: String, coordinate: CLLocationCoordinate2D, id: String) {
        self.title = title
        self.coordinate = coordinate
        self.id = id
    }
}
