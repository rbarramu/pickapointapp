//
//  SplashViewController.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 29-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Lottie
import UIKit

class SplashViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        // 1. Set animation content mode
        splashAnimation.contentMode = .scaleAspectFit

        // 2. Set animation loop mode
        splashAnimation.loopMode = .loop

        // 3. Adjust animation speed
        splashAnimation.animationSpeed = 1

        // 4. Play animation
        splashAnimation.play()

        timeWait()
    }

    @IBOutlet var splashAnimation: AnimationView!

    func showHome() {
        let storyboard = UIStoryboard(name: "TabBar", bundle: nil)
        let tabbarcontroller = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! UITabBarController
        tabbarcontroller.modalPresentationStyle = .fullScreen
        present(tabbarcontroller, animated: true, completion: nil)
    }

    // Time delay to go from one view to another
    func timeWait() {
        let when = DispatchTime.now() + 2.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.showHome()
        }
    }
}
