//
//  Constants.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 29-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import UIKit

enum AlertViewTypes {
    case addedToFavorite
    case serverError
    case noInternetAlert
}

struct Constants {
    struct Images {
        static let heartUnselectedIcon = UIImage(named: "heartUnselectedIcon")
        static let heartSelectedIcon = UIImage(named: "heartSelectedIcon")
        static let locationIcon = UIImage(named: "locationIcon")
    }
}

struct Messages {
    struct MapView {
        static let titleLabel = NSLocalizedString("Busca puntos de interés", comment: "")
        static let searchPlaceholder = NSLocalizedString("Buscar", comment: "")
    }

    struct DetailView {
        static let ratingLabel = NSLocalizedString("Rating", comment: "")
    }

    struct Api {
        static let internetText = NSLocalizedString("Sin conexión a Internet.", comment: "")
        static let serverErrorText = NSLocalizedString("Lo sentimos ha ocurrido un error con el servidor.", comment: "")
    }

    struct UIAlertView {
        static let addedToFavoriteTitle = NSLocalizedString("Añadido a tus favoritos", comment: "")
        static let addedToFavoriteSubtitle = NSLocalizedString("Se añadió correctamente a tus favoritos", comment: "")
        static let okText = NSLocalizedString("OK", comment: "")
        static let internetText = NSLocalizedString("Sin conexión a Internet.", comment: "")
        static let serverErrorText = NSLocalizedString("Lo sentimos ha ocurrido un error con el servidor.", comment: "")
    }
}

/// Constants used in Api calls.
struct Api {
    struct Server {
        /// True Development
        /// False Production
        static let debugURL = true

        /// URL base.
        static let baseURL = "https://maps.googleapis.com/maps" // Production
        static let testUrl = "https://maps.googleapis.com/maps" // Developer
        static let version = "/api/place/"
        static let places = "nearbysearch/json"
        static let details = "details/json"
        /// Key Google
        static let key = "AIzaSyA7WxOP2harzmYMPu6GjAY1itthdIwTmhk"
    }
}

struct Generals {
    static let radius = 15000
}
