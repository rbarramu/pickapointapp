//
//  ActivityIndicator.swift
//  PickAPointApp
//
//  Created by Rocio Barramuño on 31-08-20.
//  Copyright © 2020 Rocio Barramuño. All rights reserved.
//

import Foundation
import UIKit

// Used for ViewControllers that need to present an activity indicator when loading data.
public protocol ActivityIndicatorPresenter {
    /// The activity indicator
    var activityIndicator: UIActivityIndicatorView { get }

    /// Show the activity indicator in the view
    func showActivityIndicator()

    /// Hide the activity indicator in the view
    func hideActivityIndicator()
}

public extension ActivityIndicatorPresenter where Self: UIViewController {
    func showActivityIndicator() {
        view.isUserInteractionEnabled = false

        DispatchQueue.main.async
        {
            self.activityIndicator.style = .whiteLarge

            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            self.activityIndicator.backgroundColor = UIColor(red: 0.16, green: 0.17, blue: 0.21, alpha: 1)
            self.activityIndicator.layer.cornerRadius = 6
            self.activityIndicator.center = CGPoint(x: self.view.bounds.size.width / 2, y: self.view.bounds.height / 2)
            self.view.addSubview(self.activityIndicator)
            self.activityIndicator.startAnimating()
        }
    }

    func hideActivityIndicator() {
        view.isUserInteractionEnabled = true

        DispatchQueue.main.async
        {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
        }
    }
}
