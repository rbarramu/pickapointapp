# PickAPointApp

Prueba técnica para SOSAFE. 

La prueba consiste en una aplicación móvil para buscar y guardar puntos de interés, donde:

- Debe poseer una vista para buscar puntos de interés, que debe estar conformada por un mapa y un buscador. Para buscar los POI se debe usar el endpoint [Nearby search request de Google Maps](https://developers.google.com/places/web-service/search?hl=es-419#PlaceSearchRequests) (Vía Http, sin uso del SDK); cuyos resultados deben ser mostrados en el mapa en forma de "Annotations" (a elección). La búsqueda debe ser basada en las coordenadas del posicionamiento del mapa y keywords ingresados en el buscador. Para el mapa utilizar [https://developer.apple.com/documentation/mapkit](https://developer.apple.com/documentation/mapkit)
- Se debe crear una vista de detalles de los puntos de interés, donde muestre:
- Nombre del punto de interés
- Dirección
- Imagen referencia
- Rating (si lo tiene)
- Listado de las primeras 5 *Reviews* junto con la imagen del *Reviewer* (Si lo tiene) [https://developers.google.com/places/web-service/details?hl=es-419](https://developers.google.com/places/web-service/details?hl=es-419)
- Mapa con la ubicación
- Botón para guardar/borrar (Guardar como favorito)
- La opción de Guardar o borrar de favoritos, debe gatillar el guardado o el borrado de los datos en base de datos local
- Crear una vista que muestre el listado de los puntos de interés guardados, donde se pueda acceder a cada uno de los ítems, y este navegue hacia la vista del detalle del mismo.
- La vista del detalle, debe cargar la información de acuerdo donde proviene, es decir, si viene del buscador y este dato no existe como guardado en favorito, debe cargar los datos provenientes del request al API, pero si este proviene del listado (ítems guardados localmente) debe recuperar los datos de la BD local.


# Observaciones:

- Esta aplicación se desarrolló con la arquitectura Clean Swift.
- Se utilizaron las librerías Alamofire para las llamadas, Realm como base de dato local, Lottie para animaciones, SwiftFormat para la identación, orden del código y Cosmos para control de ratings.



